//图片上传预览    IE是用了滤镜。
function previewImage(file) {
  	var src;
  	if(file.files && file.files[0]) {
  		var reader = new FileReader();
  		reader.onload = function(evt) {
  			src = evt.target.result;
  			//alert(src);
  			if(avalon.vmodels[window.init_page] && avalon.vmodels[window.init_page].getImg)
			avalon.vmodels[window.init_page].getImg(src)
  		};
  		reader.readAsDataURL(file.files[0]);
  	} else //兼容IE
  	{
  		var sFilter = 'filter:progid:DXImageTransform.Microsoft.AlphaImageLoader(sizingMethod=scale,src="';
  		file.select();
  		src = document.selection.createRange().text;
  		if(avalon.vmodels[window.init_page] && avalon.vmodels[window.init_page].getImg)
		avalon.vmodels[window.init_page].getImg(src);
  		//alert(src);
  	}
}