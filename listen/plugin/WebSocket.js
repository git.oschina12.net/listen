var websocket = (function(){
	
	var websocket = null;
    var connect = function(){	 
        var command = "";
        var id = $.session.get('userId');
        var token = $.session.get('token');
        var connect_ip = "139.196.211.234";
        var connect_port = "8889"

    	
        //var msg = document.getElementById("msg");
        try{
            var readyState = new Array("正在连接", "已建立连接", "正在关闭连接", "已关闭连接");

            //new ReconnectingWebSocket('ws://....');
            var host = "ws://"+connect_ip+":"+connect_port;

            websocket = new Reconnecting(host);
            websocket.onopen = function(){
                //msg.innerHTML += "<p>Socket状态： " + readyState[websocket.readyState] + "</p>";
                console.log(readyState[websocket.readyState]);
                if(readyState[websocket.readyState]=="已建立连接"){
                    try{
                        var to = '{"command":"setId","id":"'+id+'","token":"'+token+'"}';
                        websocket.send(to);
                        // var show_to = eval('(' + to + ')');
                        // console.log(show_to.id);
                    }catch(exception){
                        // msg.innerHTML += "<p>发送数据出错</p>";
                        console.log("没有登录WebSocket权限，请检查口令！");
                    }
                }
            }
            websocket.onmessage = function(event){
            	//var scope = angular.element(main_new).scope();    
                //msg.innerHTML += "<p>接收信息： " + event.data + "</p>";
                console.log("接收信息： "+event.data);
//				scope.$apply(function(){
//					var data =$.parseJSON(event.data); 
//					scope.onmessage(data);
//				});
//				setTimeout(function(){
//			      	scope.$apply(function(){});
//				},1)
			
//				var t=setTimeout(function(){
//	            	var objDiv = document.getElementById("scrool_main");
//	    			objDiv.scrollTop = objDiv.scrollHeight;      	
//				},100)
            }
            websocket.onclose = function(){
                //msg.innerHTML += "<p>Socket状态： " + readyState[websocket.readyState] + "</p>";
                console.log("关闭信息： "+readyState[websocket.readyState]);
            }
            // msg = document.getElementById("msg");
            // msg.innerHTML += "<p>Socket状态： " + readyState[websocket.readyState] + "</p>";
        }catch(exception){
            //msg.innerHTML += "<p>有错误发生</p>";
            console.log("有错误发生");
        }
    };

    var send = function(msg){
    	//var scope = angular.element(main_new).scope();
        // var msg = document.getElementById("msg");
        // var text = document.getElementById("text").value;
        if(msg == ""){
            //msg.innerHTML += "<p>请输入一些文字</p>";
            console.log("请输入一些文字");
            return;
        }
        try{
            websocket.send(msg);
            //msg.innerHTML += "<p>发送数据:  " + text + "</p>";
            console.log("发送数据: "+msg);
        }catch(exception){
            console.log("有错误发生,在发送消息的时候！");
        }
        //document.getElementById("text").value = "";
    };

    var disconnect = function(){
        websocket.close();
    };
    
    connect();

    return {
        send : send,
        connect: connect
    };

})();