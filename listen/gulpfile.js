var gulp            = require('gulp');
var sass            = require('gulp-sass');
var minifyCss       = require('gulp-minify-css');
//var fileinclude		= require('gulp-file-include');

var paths = {
    sass: ['./modules/**/*.scss', '!./modules/ionic.app.scss'],
    html: ['./modules/**/*.html'],
    common_sass: ['./common/*.scss']
};

gulp.task('sass', function(done) {
    gulp.src(paths.sass)
        .pipe(sass())
        .pipe(gulp.dest('./modules'))
        .pipe(minifyCss({
            keepSpecialComments: 0
        }))
        .pipe(gulp.dest('./modules'))
        .on('error', sass.logError)
        .on('end', done);
        

});

gulp.task('common_sass', function(done) {
    gulp.src(paths.common_sass)
        .pipe(sass())
        .pipe(gulp.dest('./common'))
        .pipe(minifyCss({
            keepSpecialComments: 0
        }))
        .pipe(gulp.dest('./common'))
        .on('error', sass.logError)
        .on('end', done);
});

//gulp.task('include',function(){
//	gulp.src(paths.html)
//		.pipe(fileinclude({
//			prefix:'@@',
//			basepath: '@file'
//		}))
//		.pipe(gulp.dest('./modules'))
//})

gulp.task('watch', function() {
    gulp.watch([paths.sass,paths.common_sass], ['sass','common_sass']);
});

gulp.task('default', ['sass','common_sass']);