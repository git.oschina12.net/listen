/**
 * Created by tony on 2016-01-18.
 */
require.config({//第一块，配置
    baseUrl: '',
    shim:{
    	reconnecting:{
    		exports: 'reconnecting'
    	},
    	socket:{
    		deps: ['reconnecting'],
			exports: 'socket'
    	},
    	jqSession:{
    		deps: ['jquery'],
    		exports: 'jqSession'
    	},
    	echarts:{
    		deps: ['jquery'],
    		exports: 'echarts'
    	},
    	map:{
    		exports: 'map'
    	},
    	file:{
    		exports: 'file'
    	},
    	laydate:{
    		exports: 'laydate'
    	},
    	jquery:{
    		exports: 'jquery'
    	}
    },
    paths: {
        avalon: ["plugin/avalon/avalon"],//必须修改源码，禁用自带加载器，或直接删提AMD加载器模块
//      dialog: ["plugin/avalon/avalon.dialog"],
        mmHistory: 'plugin/avalon/mmHistory',
        mmRouter: 'plugin/avalon/mmRouter',
        
        text: ['plugin/require/text'],
        css: ['plugin/require/css'],
        
        domReady: 'plugin/require/domReady',
        jquery: 'plugin/jquery/jquery.min',
        Math: 'plugin/Math.uuid',
        security: 'plugin/security',
        jqSession: 'plugin/jqSession',
        echarts: 'plugin/echarts-all',
        map: 'http://api.map.baidu.com/getscript?v=2.0&ak=oxkCCVlwFuB5UDw6pqiVxC4O&services=&t=20160708193109',
        file: 'plugin/file',
        Jcrop: 'plugin/jquery.Jcrop',
        socket: 'plugin/WebSocket',
        reconnecting: 'plugin/reconnecting-websocket',
		
		init: 'common/init',
        config: 'common/config',
        servers: 'common/servers',
        laydate: 'common/laydate',
        WebSocket: 'common/WebSocket',
        
    },
    
    priority: ['text'],
});
require(['mmRouter',"domReady!","jquery","file","jqSession","servers","init","WebSocket"],function(mmRouter,domReady,jquery,file,jqSession,servers,init,WebSocket){

    if(servers.GetRequest().appId)
    	servers.storage.setItem("appId",servers.GetRequest().appId)
    	
    if(servers.GetRequest().token)
    	servers.storage.setItem("token",servers.GetRequest().token)
    
	//var token ="eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJuYmYiOjE0Njg5OTQ5NjEsImFjY291bnRUeXBlIjoiYWRtaW4iLCJ0aW1lIjoxNDY4OTk0OTcwOTY2LCJleHAiOjE0NzE1ODY5NzEsImRldmljZUlkIjoiMWFkZGU1NjgtZjkwNS00ZTFkLTk2ZTAtMmY2MGMyZGQ4MGRjIiwidXNlcklkIjoxOTQsImlhdCI6MTQ2ODk5NDk3MX0.j7uQZBPOr1lQjC5t8FNQ5PbU1ADxGSgRthX5Bh7mdp4";
    var model = avalon.define('root', function(vm) {
    	vm.content="";
        vm.random=Math.random();
        vm.mb="";
		vm.appId=$.session.get('appId');
		vm.list=[];
		vm.flagTrue="1";
		
		//window.configUrl="http://192.168.5.55:8080/freewebSys/";
		
		
//		window.configUrl="http://139.196.107.71:88/freewebSys/";		
//		window.uploadToken=servers.storage.getItem('token');
//		window.appId=vm.appId;
		
		
		
        vm.loginBack=function(){
			servers.httpPost('v3/toSys/appIndex', {
				appId : vm.appId,
			}).done(function(data) {
				if ("200" == data.code) {
					vm.flagTrue="0";
					if(data.authInfo)
						servers.authInfo(JSON.stringify(data.authInfo));
					
					vm.list=[];
					vm.list=data.menuList;
					$('.menu li').children('ol').children('li').removeClass('current');
					$('.menu li:first').children('ol').show();
					$('.menu li:first').children('ol').children('li:first').addClass('current');
					console.log(location.hash.split('/')[1]);
					window.init_page = location.hash.split('/')[1];
					$('.menu li:first').children('ol').children('li').removeClass('current');
					$('.menu li:first').children('ol').hide();
			        $("#"+window.init_page).addClass("current");
			        $("#"+window.init_page).parent('ol').show();
			      	
				} else {
					return;
				}		
			});
		};
		
		vm.clickOut = function(){
			$(this).siblings('ol').toggle();
			$(this).parent('li').siblings('li').children('ol').hide();
		};
		vm.clickIn= function(){
			$('.menu li').removeClass('current');
			$(this).addClass('current').siblings('li').removeClass('current');
		};
		
		if(vm.flagTrue==="1"){
			vm.loginBack();
		}else{
			
		}
		
		
		
		//这里的控制分页的  以后看有没有优化空间/////////////////////////////////////////start
		vm.currentPage = 1;//当前页
		vm.endRow = [];//当前页数的条数
		vm.lastPage = false;
		vm.nextPage = false;
		vm.totalPages = 1;
		vm.totalPages_arr = [];
		vm.pagingOk = "";
		//vm.page = 1;
		vm.paging = function(paginator){
			vm.totalPages_arr = [];
			for(var i = 0; i < paginator.totalPages; i++){
				vm.totalPages_arr.push({i});
            }
			vm.totalPages = paginator.totalPages;
			vm.currentPage = paginator.page;
			vm.lastPage = paginator.lastPage;
			vm.nextPage = paginator.nextPage;
			
			vm.pagingOk = "";
		};
		
		vm.paging_next = function(){
			avalon.vmodels[window.init_page].pageIndex = vm.currentPage+1;
			avalon.vmodels[window.init_page].paging_page();
		};
		
		vm.paging_last = function(){
			avalon.vmodels[window.init_page].pageIndex = vm.currentPage-1;
			avalon.vmodels[window.init_page].paging_page();
		};
		
		vm.paging_current = function(page){
			avalon.vmodels[window.init_page].pageIndex = page;
			avalon.vmodels[window.init_page].paging_page();
		};
		
		vm.paging_ok = function(){
			if(vm.pagingOk){
				if(vm.pagingOk>vm.totalPages)
				avalon.vmodels[window.init_page].pageIndex = vm.totalPages;
				else
				avalon.vmodels[window.init_page].pageIndex = vm.pagingOk;
			}
			else
			avalon.vmodels[window.init_page].pageIndex = 1;
			avalon.vmodels[window.init_page].paging_page();
		};
		//这里的控制分页的  以后看有没有优化空间/////////////////////////////////////////end
		
	});
    
    //为页面选择模板 没有选择 默认是0
    var modules = []; 
	modules['login'] = 1;
	modules['apply'] = 1;
    
    //导航回调
    function callback() {
        var jspath = "modules"; //这里改成您自己的网站地址 ,这个是js路径的变量
        var pagepath = "";       //这个是网页的变量

		//处理加载的页面 和 主js
        var paths = this.path.split("/");
        for (var i = 0; i < paths.length; i++) {
            if (paths[i] != "") {
                jspath += "/" + paths[i];
                pagepath += "_" + paths[i];
            }
        }
        
        window.init_page = pagepath.split('_')[1];
        if(modules[window.init_page])
        	avalon.vmodels.root.mb = modules[window.init_page];
        else
        	avalon.vmodels.root.mb = 0;

        require([jspath], function (page) {
            avalon.vmodels.root.content = pagepath;
            $('body .mb').css("opacity",1);
        });
    }

    avalon.log("加载avalon路由");
    avalon.router.get("/about/about", callback);//关于我们
    avalon.router.get("/navigation/navigation", callback);//门诊导航
    avalon.router.get("/DeptList/DeptList", callback);//公告管理
    avalon.router.get("/intro/intro", callback);//门诊介绍
    avalon.router.get("/send/send", callback);
    avalon.router.get("/sendList/sendList", callback);
    avalon.router.get("/newAnnouncement/newAnnouncement", callback);//新增公告消息
    avalon.router.get("/instroduction/instroduction", callback);
    avalon.router.get("/lunBo/lunBo", callback);//轮播
    avalon.router.get("/pushList/pushList", callback);//推送列表
    avalon.router.get("/dataCount/dataCount", callback);//数据统计
    avalon.router.get("/user/user", callback);//注册用户子页面
    avalon.router.get("/Information/Information", callback);//资讯列表子页面
    avalon.router.get("/process/process", callback);//验配流程
    avalon.router.get("/addExpert/addExpert", callback);//添加专家
    avalon.router.get("/newsList/newsList", callback);//资讯列表
   	avalon.router.get("/mavinList/mavinList", callback);//专家列表
   	avalon.router.get("/regList/regList", callback);//注册用户列表
   	avalon.router.get("/suggestList/suggestList", callback);//意见反馈
   	avalon.router.get("/freePhone/freePhone", callback);//免费通话
    avalon.router.get("/addMess/addMess", callback);//新增推送
    avalon.router.get("/phoneDetail/phoneDetail", callback);//免费通话查看详情
    avalon.router.get("/departList/departList", callback);//设备列表
    avalon.router.get("/details/details", callback);//意见反馈子页面
    avalon.router.get("/addEquip/addEquip", callback);//添加设备
    avalon.router.get("/addLunbo/addLunbo", callback);//添加轮播
    avalon.router.get("/appointment/appointment", callback);//预约列表
    avalon.router.get("/schedule/schedule", callback);//排班表
    
    avalon.history.start({
        basepath: "/avalon",
    });
    if(!window.init_page){
    	avalon.router.navigate("/intro/intro"); //默认打开首页 avalon.scan();
    }
    avalon.scan();
});