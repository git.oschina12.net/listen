define(['avalon', 'text!./lunBo.html','config','servers','css!./lunBo.css','jquery',], function (avalon,html,config,servers) {
    avalon.templateCache._lunBo_lunBo= html;
	 var lunBo = avalon.define('lunBo', function(vm) { 
		vm.addLunbo= function(){
			avalon.router.navigate("/addLunbo/addLunbo");
		};
		
		vm.pageIndex="";
		vm.delflag="";
		vm.title="";
		vm.id="";
		vm.typeId="";
		vm.pageNo="";
		vm.list=[];
		vm.pub=[];
		vm.lunboList = function() {
			servers.httpPost('v3/picscroll/list', {
				pageIndex:vm.pageIndex,
				delflag:vm.delflag,
				title:vm.title
			}).done(function(data) {
				if (data.code == "200") {
					vm.list=[];
					vm.list = data.body;
				} else {
					
				}
			});
		};
		vm.editMess = function(id){
	    	avalon.router.navigate("/addLunbo/addLunbo?id="+id);
	   	};
	   	vm.publish = function(id,cou) {
			servers.httpPost('v3/picscroll/publish', {
				id:id,
				typeId:cou,
				pageNo:vm.pageNo,
			}).done(function(data) {
				if (data.code == "200") {
					vm.pub=[];
					vm.pub = data.body;
					vm.lunboList();
				} else {
					alert(data.msg);
				}
			});
		};
	   	
		vm.removeRemote = function($index, $remove,id) {
			servers.httpPost('v3/picscroll/deleteById', {
				id : id,
			}).done(function(data) {
				if (data.code == "200") {
                	$remove();
				}else{
					alert(data.msg);
				}
			});
    };		
		vm.init=function(){
    		 vm.lunboList();
//  		 vm.publish();
    	};
	});
	servers.init(lunBo);
                        
});