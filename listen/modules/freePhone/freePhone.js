define(['avalon','text!./freePhone.html','config','servers','laydate','jquery','css!./freePhone.css','mmRouter'], function (avalon,page,config,servers,laydate) {
    avalon.templateCache._freePhone_freePhone = page;
    var freePhone = avalon.define('freePhone', function(vm) {
    	vm.startDate="";
    	vm.endDate="";
    	vm.telephone="";
    	vm.id="";
    	vm.lis=[];
    	vm.tabs = ["tab0", "tab1","tab2"];
	    vm.currentIndex=0;
	    vm.delflag = "t";
		
		 vm.selected=[];
		lang = [],
		vm.ids = ["10701","10700"];
        vm.toggle=function(index,delflag) {
           vm.currentIndex = index;
           $(this).addClass("tabCurrent").siblings().removeClass("tabCurrent");
           vm.delflag = delflag;
		   vm.freePhone();
        };        
        vm.freePhone=function(){
     	 	servers.httpPost('v3/freeregphone/list', {
				startDate : vm.startDate,
				endDate:vm.endDate,
				telephone:vm.telephone,
				delflag:vm.delflag
			}).done(function(data) {
				if (data.code == "200") {
					vm.lis=[];
					vm.lis=data.body;
			    }
			});
     	 };
        vm.init=function(){
	    	vm.freePhone();
	    };
        vm.freeDetail=function(id){
        	avalon.router.navigate("/phoneDetail/phoneDetail?id="+id);
        };      
        vm.removeRemote = function($index, $remove,id) {
			servers.httpPost('v3/freeregphone/deleteById', {
				id : id,
			}).done(function(data) {
				if (data.code == "200") {
                	$remove();
				}else{
					alert(data.msg)
				}
			});
    	};
        
        vm.phoneUpdate = function(id) {
			servers.httpPost('v3/freeregphone/updateById', {
				id : id,
			}).done(function(data) {	
				if (data.code == "200") {
					alert("已处理");
					vm.appointmentList();
				} else {
					alert("失败");
				}
			});
		};
        
        vm.allDel = function(){
        	 servers.httpPost('v3/freeregphone/deleteBatchByIds', {
				ids : ids
			}).done(function(data) {
				if (data.code == "200") {
					alert("成功");
				} else {
					alert("失败");
				}
			});
       };
        //资料来源 http://www.jb51.net/article/65529.htm
		vm.selected=[];//保存勾选的选项的id,方便传给后台
	    //vm.list=[{id:1,text:'aaa'},{id:2,text:'bbb'},{id:3,text:'ccc'},{id:4,text:'ddd'},{id:5,text:'eee'},{id:6,text:'fff'}];
	    vm.lis=[{}];
	    vm.select_all_cb=function(){//全选框change事件回调
//	        var list=vm.list,selected=vm.selected; 
	        var lis=freePhone.lis,selected=freePhone.selected;
	        if(this.checked){
				avalon.each(lis,function(i,v){//循环保存着已经勾选选框的数据
					selected.ensure(v['id']);//如果里面没有当前选框的数据，就保存
				});
	        }else
	        selected.clear();//清空
	    };
	    vm.select_all=0;
	    
    });
    freePhone.selected.$watch('length',function(after){//监听保存数据数组的变化
      var len=freePhone.lis.length;      
      if(after==len)//子选框全部被勾选
        freePhone.select_all=1;
      else//子选框有一个没有被勾选
        freePhone.select_all=0;
    });
    servers.init(freePhone);
});