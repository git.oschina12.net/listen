define(['avalon', 'text!./newsList.html','config','servers','css!./newsList.css','jquery',], function (avalon,html,config,servers) {
    avalon.templateCache._newsList_newsList= html;
	var newsList = avalon.define('newsList', function(vm) { 
		vm.pageIndex="1";
		vm.infoName="";
		vm.delflag="";
		vm.id = "";
		vm.list=[];
		vm.selectcb="";
		vm.select= "";
		vm.paging = "modules/pluginPaging/paging.html";
		
		
	    vm.click = function(id){
	    	avalon.router.navigate("/Information/Information?id="+id);
	    };
	    vm.click2 = function(){
	    	avalon.router.navigate("/Information/Information");
	    };
        vm.Information = function() {
			servers.httpPost('v3/article/getArticleList', {
				infoName : vm.infoName,
				pageIndex: vm.pageIndex,
				delflag: vm.select
			}).done(function(data) {
				 var arr = data;
				if (arr.code == "200") {
					vm.list=arr.body;
					//paging(arr.paginator);
					avalon.vmodels.root.paging(arr.paginator);
			    }
			});
		};
		vm.updateArticle = function(id,cout) {
			servers.httpPost('v3/article/updateArticle', {
				id :id,
				flag:cout
			}).done(function(data) {
				if (data.code == "200") {
					vm.Information();
			    }else{
					alert(data.msg)
				}
			});
		};
//	    vm.selectchange=function(){
//          vm.Information();
//     	};
	    vm.removeRemote = function($index, $remove,id) {
			servers.httpPost('v3/article/deleteArticleById', {
				id : id,
			}).done(function(data) {
				if (data.code == "200") {
                	$remove();
				}else{
					alert(data.msg)
				}
			});
   };
	    vm.init = function(){
			vm.select = "";
			vm.Information();
		};
		vm.paging_page = function(){
			vm.Information();
		};
		
		
    });
    servers.init(newsList);
});