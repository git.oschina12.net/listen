define(['avalon', 'text!./mavinList.html','config','servers','css!./mavinList.css','jquery','Jcrop'], function (avalon,html,config,servers) {
    avalon.templateCache._mavinList_mavinList = html;
    var mavinList = avalon.define('mavinList', function(vm) { 
  	    vm.modal="modules/pluginModal/modal.html";
     	vm.arr=[];
     	vm.page = "1";
     	vm.name = "";
     	vm.title = "";
     	vm.limit ="10";
		vm.newMavin = function(){
			avalon.router.navigate("/addExpert/addExpert");
		};
		vm.checkMavin = function(id){
			avalon.router.navigate("/addExpert/addExpert?id="+id);
		};
		vm.upList = function(){
			servers.httpPost('v3/doctor/list', {
				page:vm.page,
				name : vm.name,
                title : vm.title,
				limit:vm.limit,
			}).done(function(data) {
				if (data.code == "200"){
					vm.arr= data.body;
				} else {
					alert(data.msg);
				}
			});
		};
        vm.del = function($index, $remove,id) {
				servers.httpPost('v3/doctor/del', {
					id : id,
				}).done(function(data) {
					if (data.code == "200") {
	                	$remove();
					}else{
						alert(data.msg)
					}
				});
	    };   
	   vm.init=function(){
		 vm.upList();
		
	   };
	});
     servers.init(mavinList);
});