define(['avalon', 'text!./user.html', 'config', 'servers', 'css!./user.css', 'jquery', ], function(avalon, html, config, servers) {
	avalon.templateCache._user_user = html;
	var user = avalon.define('user', function(vm) {
		vm.tabs = ["tab0", "tab1"];
		vm.currentIndex = 0;
		vm.user ={};
		vm.reg = [];
		vm.id="";
    	vm.qn="";
		vm.toggle = function(index) {
			vm.currentIndex = index;
			$(this).addClass("current").siblings().removeClass("current");
			if(index==0){
				$(".conten").text("用户信息");
			}else{
				$(".conten").text("预约记录");
			}
		};
		vm.regList = function() {
			servers.httpPost('v3/users/selectUserById', {
				userid : servers.GetRequest().userid
			}).done(function(data) {
				if (data.code == "200") {
					vm.qn = config.qn_domain;
					vm.user=[]
					vm.user=data.body;
				} else {
					alert("失败");
				}
			});
		};
		
        vm.regList();
        
        vm.regList2= function() {
			servers.httpPost('v3/users/getBookingregList', {
				userid : servers.GetRequest().userid
			}).done(function(data) {
				if (data.code == "200") {
					vm.reg=data.body;
				} else {
					alert("失败");
				}
			});
		};
        vm.regList2();
        //如果出现删除状态，执行此函数
        vm.removeRemote = function(id) {
			servers.httpPost('v3/users/getBookingregList', {
				id : id,
			}).done(function(data) {
				if (data.code == "200") {
                	$remove();
				}else{
					alert(data.msg)
				}
			});
    	};
        
        vm.init = function(){
        	vm.regList();
        };
	});
	
	servers.init(user);
});