define(['avalon', 'text!./regList.html','config','servers','css!./regList.css','jquery',], function (avalon,html,config,servers) {
    avalon.templateCache._regList_regList= html;
	var regList = avalon.define('regList', function(vm) {
		vm.username="";
		vm.pageIndex="1";
		vm.list=[];
     	vm.click=function(id){
     	 	avalon.router.navigate("/user/user?userid="+id);
     	};
        vm.regList = function() {
			servers.httpPost('v3/users/getUsersList', {
				username : vm.username,
				pageIndex:vm.pageIndex
			}).done(function(data) {
				if (data.code == "200") {
					vm.list=data.body;
				} else {
					alert(data.msg);
				}
			});
		};
        vm.init = function(){
        	vm.regList();
        };
        //vm.regList();
	});
	//	avalon.scan();
    servers.init(regList);
});