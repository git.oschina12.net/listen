define(['avalon', 'text!./about.html','config','servers','css!./about.css','jquery',], function (avalon,html,config,servers) {
    avalon.templateCache._about_about = html;
	var about = avalon.define('about', function(vm) { 
	vm.id="";
    vm.infuse="";
    vm.aboutList = function() {
		servers.httpPost('v3/appimage/selectAboutUs', {
		}).done(function(data) {
			if (data.code == "200") {
				vm.infuse = data.body.infuse;
				vm.id=data.body.id;
			} else {
				
			}
		});
	};
	
	vm.saveAbout = function() {
		servers.httpPost('v3/appimage/saveAboutUs', {
			id:vm.id,
			infuse:vm.infuse
		}).done(function(data) {
			if (data.code == "200") {
				
			} else {
				
			}
		});
	};
	
	vm.init = function() {
		vm.aboutList();
	};
	
 });   
 
 servers.init(about);
});