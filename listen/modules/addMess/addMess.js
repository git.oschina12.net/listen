define(['avalon', 'text!./addMess.html','config','servers','css!./addMess.css','jquery',], function (avalon,html,config,servers) {
    avalon.templateCache._addMess_addMess= html;

	var addMess = avalon.define('addMess', function(vm) {
		vm.pageIndex="1";
		vm.infoName="";
		vm.delflag="t";
		vm.title="";
		vm.name="";
		vm.content="";
		vm.id="";
		vm.type="";
		vm.targetType="";
		vm.targetId="";
		vm.news=[];
		vm.doctors=[];
		vm.device=[];
		vm.lookpush={};
		vm.tabs = ["tab0", "tab1", "tab2"];
		vm.tab1=["tab0","tab1"]
		vm.title_tag = true;
		vm.select = 2;
		vm.toggle = function(index,type) {
			vm.targetType = type;
		}
		vm.openList=function(){
			vm.zixunList();
		};
		vm.look = function() {
			servers.httpPost('v3/notification/selectById', {
				id:servers.GetRequest().id,
			}).done(function(data) {
				if (data.code == "200") {
					if(data.body){
						vm.content = data.body.content;
						vm.title = data.body.title;
						vm.select = data.body.type;
						vm.targetId = data.body.targetId;
						vm.targetType = data.body.targetType;
					}else{
						vm.content = "";
						vm.title= "";
						vm.select = 2;
						vm.targetId = "";
						vm.targetType = 1;
					}
				} else {
					alert("登陆失败");
				}
			});
		};		
		vm.saveOrUpdate=function(){
			servers.httpPost('v3/notification/saveOrUpdate', {
				id:servers.GetRequest().id,
				title:vm.title,
				content:vm.content,
				type:vm.select,
				targetType:vm.targetType,
				targetId:vm.targetId
			}).done(function(data) {
				if ("200" == data.code) {
					avalon.router.navigate("/pushList/pushList");
				} else {
					return;
				}		
			});
		};		
		vm.backList=function(){
			avalon.router.navigate("/pushList/pushList");
		};
		vm.zixunList = function() {
			servers.httpPost('v3/article/getArticleList', {
				infoName : vm.infoName,
				pageIndex:vm.pageIndex,
				delflag:vm.delflag
			}).done(function(data) {
				if (data.code == "200") {
					vm.news=data.body;
			    }
			});
		};
		vm.doctorList = function() {
			servers.httpPost('v3/doctor/list', {
				name : vm.name,
				pageIndex:vm.pageIndex,
			}).done(function(data) {
				if (data.code == "200") {
					vm.doctors=data.body;
			    }
			});
		};
		vm.deviceList = function() {
			servers.httpPost('v3/goods/getDeviceList', {
				name : vm.name,
				pageIndex:vm.pageIndex,
			}).done(function(data) {
				if (data.code == "200") {
					vm.device=data.body;
			    }
			});
		};
        vm.init = function(){
        	vm.title_tag = (servers.GetRequest().id == null);
			vm.look();
			vm.zixunList();
	        vm.doctorList();
	        vm.deviceList();
        };
	});
    servers.init(addMess);                   
});