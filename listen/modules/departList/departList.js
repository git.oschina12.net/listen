define(['avalon', 'text!./departList.html','config','servers','css!./departList.css','jquery',], function (avalon,html,config,servers) {
    avalon.templateCache._departList_departList = html;
    var departList = avalon.define('departList', function(vm) {
    	vm.pageIndex="1";
    	vm.name="";
    	vm.list=[];
    	vm.qn="";
    	 vm.addEquip = function(){
	    	avalon.router.navigate("/addEquip/addEquip");
	    };
	    vm.getDeviceList=function(){
			servers.httpPost('v3/goods/getDeviceList', {
				pageIndex:vm.pageIndex,
				name:vm.name
			}).done(function(data) {
				if ("200" == data.code) {
					vm.list=data.body;
					vm.qn = config.qn_domain;
				} else {
					alert(data.msg);
				}		
			});
		};
		vm.editDetail= function(id){
			avalon.router.navigate("/addEquip/addEquip?editNo=0&&id="+id);
		};
		vm.lookDetail= function(id){
			avalon.router.navigate("/addEquip/addEquip?editNo=1&&id="+id);
		};
		vm.removeDetail=function($index, $remove,id){
			servers.httpPost('v3/goods/delDeviceById', {
				id:id
			}).done(function(data) {
				if ("200" == data.code) {
					$remove();
				} else {
					return;
				}		
			});
		};
		vm.init = function(){
        	vm.getDeviceList();
       };
		
   });      
   servers.init(departList);
});