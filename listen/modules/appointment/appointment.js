define(['avalon', 'text!./appointment.html', 'config', 'servers', 'css!./appointment.css', 'jquery', ], function(avalon, html, config, servers) {
	avalon.templateCache._appointment_appointment = html;
	var appointment = avalon.define('appointment', function(vm) {
		vm.tabs = ["tab0", "tab1","tab2"];
		vm.currentIndex = 0;
		vm.appoint = [];
		vm.id = "";
		vm.delflag = "t";
		lang = [],
		vm.ids = [];
		vm.select_all=0;
		
		
		vm.toggle = function(index,delflag) {
			vm.currentIndex = index;
			$(this).addClass("current").siblings().removeClass("current");
			vm.delflag = delflag;
			vm.appointmentList();
		};
		vm.appointmentList = function() {
			servers.httpPost('v3/bookingreg/list', {
				userid : servers.getUser().userId,
				delflag : vm.delflag
			}).done(function(data) {
				if (data.code == "200") {
					vm.appoint = [];
					vm.appoint = data.body;
				} else {
					alert("失败");
				}
			});
		};
		vm.pending = function(id) {
			servers.httpPost('v3/bookingreg/updateById', {
				id : id,
			}).done(function(data) {	
				if (data.code == "200") {
					alert("已处理");
					vm.appointmentList();
				} else {
					alert("失败");
				}
			});
		};
        vm.allDel = function(){
        	 servers.httpPost('v3/bookingreg/updateBatchByIds', {
				ids : ids
			}).done(function(data) {
				if (data.code == "200") {
					alert("成功");
				} else {
					alert("失败");
				}
			});
       };
        //资料来源 http://www.jb51.net/article/65529.htm
		vm.selected=[];//保存勾选的选项的id,方便传给后台
	    //vm.list=[{id:1,text:'aaa'},{id:2,text:'bbb'},{id:3,text:'ccc'},{id:4,text:'ddd'},{id:5,text:'eee'},{id:6,text:'fff'}];
	    vm.list=[];
	    vm.select_all_cb=function(){//全选框change事件回调
	        var list=vm.appoint,selected=vm.selected;
	        if(this.checked){
				avalon.each(vm.appoint,function(i,v){//循环保存着已经勾选选框的数据
					selected.ensure(v['id']);//如果里面没有当前选框的数据，就保存
				});
	        }else
	        selected.clear();//清空
	    };
	    vm.init=function(){
	    	vm.appointmentList();
	    };
	});
	appointment.selected.$watch('length', function(after) { //监听保存数据数组的变化
		var len = appointment.appoint.length;
		if(after == len) //子选框全部被勾选
			appointment.select_all = 1;
		else //子选框有一个没有被勾选
			appointment.select_all = 0;
	});
	servers.init(appointment);
});