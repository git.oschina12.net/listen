define(['avalon','text!./dataCount.html','config','servers','laydate','jquery','css!./dataCount.css','mmRouter','echarts'], function (avalon,page,config,servers,laydate) {
    avalon.templateCache._dataCount_dataCount = page;
    var dataCount = avalon.define('dataCount', function(vm) {
    	vm.tabs = ["tab0", "tab1","tab2"];
	    vm.currentIndex=0;
	    vm.startDate ="2016-06-01";
	    vm.endDate = "2016-07-1";
	    vm.startDate1 ="2016-06-01";
	    vm.endDate1 = "2016-07-1";
	    vm.startDate2 ="2016-06-01";
	    vm.endDate2 = "2016-07-1";
	    vm.startDate3 ="2016-06-01";
	    vm.endDate3 = "2016-07-1";
	    vm.oldtoday = "";
	    vm.istoday = "";
	    vm.today = "";
	    vm.user = "";
	    vm.regToday ="2016-07-1";
	    vm.regToday1 = laydate.now(0);
	    vm.regToday = laydate.now(0);
	    vm.iosCount ="";
	    vm.androidCount = "";
	    vm.bothCount ="";
	    vm.iosCount1 ="";
	    vm.androidCount1 = "";
	    vm.bothCount1 ="";
	    vm.x = 0;
	    vm.y=0;
        vm.toggle=function(index) {
           vm.currentIndex = index;
           $(this).addClass("tabCurrent").siblings().removeClass("tabCurrent");
        };
        vm.GetDateStr = function(AddDayCount) {
			var dd = new Date();
			dd.setDate(dd.getDate()+AddDayCount);//获取AddDayCount天后的日期
			var y = dd.getFullYear();
			var m = dd.getMonth()+1;//获取当前月份的日期
			var d = dd.getDate();
			return y+"-"+m+"-"+d;
		};
      //   今日注册数量
        vm.listDay = function() {
			servers.httpPost('v3/userLiving/RegsiterList', {
				startDate : vm.startDate1,
				endDate : vm.endDate1,
			}).done(function(data) {
				if(data.code == "200"){
					for (var i = 0; i < data.body.length; i++) {
						options.xAxis[0].data[i] = data.body[i].dateS;
						options.series[0].data[i] = data.body[i].bothCount;
					}
					myChart1.clear();
					myChart1.setOption(options);
				} else {
					// alert("data.msg");
				}
			});
		};
		//   用户统计 --注册趋势
		vm.userList = function() {
			servers.httpPost('v3/userLiving/userList', {
				startDate : vm.startDate2,
				endDate : vm.endDate2,
			}).done(function(data) {
				if(data.code == "200"){
						for (var i = 0; i < data.body.length; i++) {
							option.xAxis[0].data[i] = data.body[i].dateS;
							option.series[0].data[i] = data.body[i].iosCount;
							option.series[1].data[i] = data.body[i].androidCount;
						}
						myChart2.clear();
						myChart2.setOption(option);
				} else {
					// alert("data.msg");
				}
			});
		};
		//   今日用户数量
		vm.RegsiterList = function() {
			servers.httpPost('v3/userLiving/userList', {
				startDate : vm.startDate,
				endDate : vm.endDate,
			}).done(function(data) {
				if(data.code == "200"){
					for (var i = 0; i < data.body.length; i++) {
						options.xAxis[0].data[i] = data.body[i].dateS;
						options.series[0].data[i] = data.body[i].bothCount;
					}
					myChart.clear();
					myChart.setOption(options);
				} else {
					// alert("data.msg");
				}
			});
		};
		//今日注册趋势
		vm.userDay = function() {
			servers.httpPost('v3/userLiving/RegsiterList', {
				startDate : vm.startDate3,
				endDate : vm.endDate3,
			}).done(function(data) {
				if(data.code == "200"){
						for (var i = 0; i < data.body.length; i++) {
							option.xAxis[0].data[i] = data.body[i].dateS;
							option.series[0].data[i] = data.body[i].iosCount;
							option.series[1].data[i] = data.body[i].androidCount;
						}
						myChart3.clear();
						myChart3.setOption(option);
				} else {
					// alert("data.msg");
				}
			});
		};
		vm.RegsiterList1 = function() {
			servers.httpPost('v3/userLiving/userList', {
				startDate : vm.istoday,
				endDate : vm.istoday,
			}).done(function(data) {
				if(data.code == "200"){
						vm.today= data.body[0].bothCount;
//						return vm.allDate
				} else {
					// alert("data.msg");
				}
			});
		};
		vm.listDay1 = function() {
			servers.httpPost('v3/userLiving/RegsiterList', {
				startDate : vm.istoday,
				endDate : vm.istoday,
			}).done(function(data) {
				if(data.code == "200"){
						vm.user= data.body[0].bothCount;
//						return vm.allDate
				} else {
					// alert("data.msg");
				}
			});
		};
		vm.userList1 = function() {
			servers.httpPost('v3/userLiving/userList', {
				startDate : vm.regToday,
				endDate : vm.regToday,
			}).done(function(data) {
				if(data.code == "200"){
							vm.iosCount = data.body[0].iosCount;
							vm.androidCount = data.body[0].androidCount;
							vm.bothCount= data.body[0].bothCount;
				}
			});
		};
		vm.userDay1 = function() {
			servers.httpPost('v3/userLiving/RegsiterList', {
				startDate : vm.regToday1,
				endDate : vm.regToday1,
			}).done(function(data) {
				if(data.code == "200"){
							vm.iosCount1 = data.body[0].iosCount;
							vm.androidCount1 = data.body[0].androidCount;
							vm.bothCount1= data.body[0].bothCount;
				}
			});
		};
		vm.pre=function(){
			vm.regToday1=laydate.now(vm.y-1);
		    vm.y--;
		};
		vm.textCount =function(){
			 s2  =  laydate.now(0)  
             s1  =  vm.regToday  
             s3  = vm.regToday1 
             var first = new Date(s1.replace(/-/g,"/"));
             var second = new Date(s2.replace(/-/g,"/"));
             var tree = new Date(s3.replace(/-/g,"/"));
            vm.x = (first-second)/(24*60*60*1000)
            vm.y = (tree-second)/(24*60*60*1000)
		}
	     function  DateDiff(sDate1,  sDate2){    //sDate1和sDate2是2006-12-18格式  
	       var  aDate,  oDate1,  oDate2,  iDays  
	       aDate  =  sDate1.split("-")  
	       oDate1  =  new  Date(aDate[1]  +  '-'  +  aDate[2]  +  '-'  +  aDate[0])    //转换为12-18-2006格式  
	       aDate  =  sDate2.split("-")  
	       oDate2  =  new  Date(aDate[1]  +  '-'  +  aDate[2]  +  '-'  +  aDate[0])  
	       iDays  =  parseInt(Math.abs(oDate1  -  oDate2)  /  1000  /  60  /  60  /24)    //把相差的毫秒数转换为天数  
	      return  iDays
	    }    
		vm.next=function(){
           vm.regToday1=laydate.now(vm.y+1)
           vm.y++;
		};
        vm.pre1=function(){
			vm.regToday=laydate.now(vm.x-1);
		    vm.x--;
		};
		vm.next1=function(){
           vm.regToday=laydate.now(vm.x+1)
           vm.x++;
		};
     vm.AddDays=function(d, n) {
	     var t = new Date(d);//复制并操作新对象，避免改动原对象
	     t.setDate(t.getDate() + n);
	     return t;
	 };
		vm.init = function(){
		    vm.listDay();
		    vm.userList();
            vm.RegsiterList();
            vm.userDay();
            vm.GetDateStr();
            vm.oldtoday =vm.GetDateStr(-1);
            vm. istoday = vm.GetDateStr(0);
            vm.RegsiterList1();
            vm.listDay1();
            vm.userList1();
            vm.userDay1();
		};
    });
     servers.init(dataCount);     
});