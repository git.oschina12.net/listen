        xdata = [0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23];
    	gender = [0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23];
    	createby = [1,1,1,1,1,1,1,1,1,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23];
    	
			 var myChart = echarts.init(document.getElementById('registerData')); 
		     var myChart1 = echarts.init(document.getElementById('userData'));
		     var myChart2 = echarts.init(document.getElementById('tendency'));
		     var myChart3 = echarts.init(document.getElementById('statistics'));
		     
	       option = {
	            tooltip: {
	                show: true
	            },
	            legend: {
	                data:['Android','Iphone']
	            },
	            xAxis : [
	                {
	                    type : 'category',
	                    data : xdata
				}],
				yAxis : [ {
					type : 'value'
				} ],
				toolbox: {
			        show : true,
			        feature : {
			            dataView : {show: false, readOnly: false},
			            magicType : {show: true, type: ['line', 'bar']},
			            restore : {show: true},
			            saveAsImage : {show: true}
			        }
			    },
				series : [ {
					"name" : "IOS注册量",
					"type" : "line",
					"data" : gender
				},{
					"name" : "android注册量",
					"type" : "line",
					"data" : createby
				}]
			};
			  myChart.setOption(option);
			  myChart2.setOption(option);
    	      myChart3.setOption(option);
    	      
    	      options = {
//				    title : {
//				        text: '某楼盘销售情况',
//				        subtext: '纯属虚构'
//				    },
				    tooltip : {
				        trigger: 'axis'
				    },
//				    legend: {
//				        data:['意向','预购','成交']
//				    },
				    toolbox: {
				        show : false,
				        feature : {
				            mark : {show: true},
				            dataView : {show: true, readOnly: false},
				            magicType : {show: true, type: ['line', 'bar', 'stack', 'tiled']},
				            restore : {show: true},
				            saveAsImage : {show: true}
				        }
				    },
				    calculable : true,
				    xAxis : [
				        {
				            type : 'category',
				            boundaryGap : false,
				            data : ['1','2','3','4','5','6','6']
				        }
				    ],
				    yAxis : [
				        {
				            type : 'value'
				        }
				    ],
				    series : [
//				        {
//				            name:'成交',
//				            type:'line',
//				            smooth:true,
//				            itemStyle: {normal: {areaStyle: {type: 'default'}}},
//				            data:[10, 12, 21, 54, 260, 830, 710]
//				        },
//				        {
//				            name:'预购',
//				            type:'line',
//				            smooth:true,
//				            itemStyle: {normal: {areaStyle: {type: 'default'}}},
//				            data:[30, 182, 434, 791, 390, 30, 10]
//				        },
				        {
				            name:'意向',
				            type:'line',
				            smooth:true,
				            itemStyle: {normal: {areaStyle: {type: 'default'}}},
				            data:[1320, 1132, 601, 234, 120, 90, 20]
				        }
				    ]
				};
//      }
            myChart1.setOption(options);