define(['avalon', 'text!./newAnnouncement.html','config','servers','css!./newAnnouncement.css','jquery',], function (avalon,html,config,servers) {
    avalon.templateCache._newAnnouncement_newAnnouncement = html;
     var newAnnouncement = avalon.define('newAnnouncement', function(vm) { 
     	vm.list=[];
     	vm.title_tag = true;
     	vm.announcement = {};
		vm.newAnnouncementList=function(){
			servers.httpPost('v3/messages/selectById', {
				id:servers.GetRequest().id,
			}).done(function(data) {
				if ("200" == data.code) {
					if(data.body){
						vm.announcement = data.body;
					}else{
						vm.announcement = {};
					}
				} else {
					return;
				}		
			});
		};
		vm.saveOrUpdate=function(){
			servers.httpPost('v3/messages/saveOrUpdate',{
				
				mid:vm.announcement.mid,
				title:vm.announcement.title,
				content:vm.announcement.content
				
				
				
			}).done(function(data) {
				if ("200" == data.code) {
					avalon.router.navigate("/DeptList/DeptList");
				} else {
					alert(data.msg);
				}		
			});
		};
		vm.backList=function(){
			avalon.router.navigate("/DeptList/DeptList");
		};
		vm.init= function(){
			vm.title_tag = (servers.GetRequest().id == null);
			vm.newAnnouncementList();
		};
	});
    servers.init(newAnnouncement);                 
});