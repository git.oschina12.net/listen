define(['avalon', 'text!./addLunbo.html','config','servers','css!./addLunbo.css','jquery',], function (avalon,html,config,servers) {
    avalon.templateCache._addLunbo_addLunbo= html;
	var addLunbo = avalon.define('addLunbo', function(vm) {
		vm.tabs = ["tab0", "tab1", "tab2"];
		vm.currentIndex = 0;
		vm.toggle = function(index) {
			vm.currentIndex = index;
			$(this).addClass("current").siblings().removeClass("current");
		};
		vm.openList=function(){
			$('.linkCon').toggle();
			vm.getArticleList();
		};
		vm.list=[];
		vm.pageIndex="";
		vm.infoName="";
		vm.delflag="t";
		vm.pageNo = "";
		vm.title= "";
		vm.pic = "";
		vm.articleId = "";
		vm.targetType = 1;
		vm.articleList=[];
		vm.doctorList=[];
		vm.name="";
		vm.deviceList=[];
		vm.title_tag = true;
		vm.paganation=1;
		vm.toggle = function(index,type) {
			vm.targetType = type;
		};
		vm.Edit = function() {
			servers.httpPost('v3/picscroll/selectById', {
				id:servers.GetRequest().id,
			}).done(function(data) {
				if (data.code == "200") {
					if(data.body){
						vm.title = data.body.title;
						vm.pageNo = data.body.pageNo;
						vm.pic = data.body.pic;
						vm.articleId= data.body.articleId;
						vm.targetType = data.body.targetType;
					}else{
						vm.pageNo = "";
						vm.title= "";
						vm.pic = "";
						vm.articleId = "";
						vm.targetType = 1;
					}
				} else {
					alert("登陆失败");
				}
			});
		};		
		vm.pageList = function() {
			servers.httpPost('v3/picscroll/page_list', {
				id:servers.GetRequest().id,
			}).done(function(data) {
				if (data.code == "200") {
					vm.list = data.body;
				} else {
					
				}
			});
		};
		
		vm.getArticleList = function() {
			servers.httpPost('v3/article/getArticleList', {
				pageIndex:vm.pageIndex,
				infoName:vm.infoName,
				delflag:vm.delflag
			}).done(function(data) {
				if (data.code == "200") {
					vm.articleList=[];
					vm.articleList = data.body;
				} else {
					
				}
			});
		};
		
		vm.DoctorList = function() {
			servers.httpPost('v3/doctor/list', {
				pageIndex:vm.pageIndex,
				name:vm.name
			}).done(function(data) {
				if (data.code == "200") {
					vm.doctorList=[];
					vm.doctorList = data.body;
				} else {
					
				}
			});
		};
		
		vm.DeviceList = function() {
			servers.httpPost('v3/goods/getDeviceList', {
				pageIndex:vm.pageIndex,
				name:vm.name
			}).done(function(data) {
				if (data.code == "200") {
					vm.deviceList=[];
					vm.deviceList = data.body;
				} else {
					
				}
			});
		};
		vm.saveOrUpdate=function(){
			servers.httpPost('v3/picscroll/saveOrUpdate', {
				id:servers.GetRequest().id,
				title:vm.title,
				pic:vm.pic,
				paganation:vm.paganation,
				pageNo:vm.pageNo,
				targetType:vm.targetType,
				articleId:vm.articleId
			}).done(function(data) {
				if ("200" == data.code) {
					avalon.router.navigate("/lunBo/lunBo");
				} else {
					return;
				}		
			});
		};	
		vm.backList=function(){
			avalon.router.navigate("/lunBo/lunBo");
		};
		vm.init=function(){
			vm.title_tag = (servers.GetRequest().id == null);
			vm.Edit();
			vm.getArticleList();
			vm.pageList();
			vm.DoctorList();
			vm.DeviceList();
		};
	});
     servers.init(addLunbo);                   
});