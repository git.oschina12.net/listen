define(['avalon', 'text!./phoneDetail.html','config','servers','css!./phoneDetail.css','jquery',], function (avalon,html,config,servers) {
    avalon.templateCache._phoneDetail_phoneDetail = html;
     var phoneDetail = avalon.define('phoneDetail', function(vm) { 
     	vm.list={};
     	vm.id="";
     	vm.regdatetime="";
     	vm.telphone="";
     	vm.content="";
     	vm.issuedesc="";
		vm.delflag="t";
		vm.lookDetail=function(){
			servers.httpPost('v3/freeregphone/selectById', {
				regdatetime:vm.regdatetime,
				telphone:vm.telphone,
				content:vm.content,
				issuedesc:vm.issuedesc,
				delflag:vm.delflag,
				id:servers.GetRequest().id,
			}).done(function(data) {
				if ("200" == data.code) {
//					vm.list=[];
					vm.list=data.body;
				} else {
					return;
				}		
			});
		};
		
//		vm.saveOrUpdate=function(){
//			servers.httpPost('v3/messages/saveOrUpdate', {
//				mid:vm.id,
//				title:vm.title,
//				content:vm.content
//			}).done(function(data) {
//				if ("200" == data.code) {
//					alert(1);
//				} else {
//					return;
//				}		
//			});
//		};
		
		vm.init=function(){
			vm.lookDetail();
		};
		
	});
	servers.init(phoneDetail);
                        
});