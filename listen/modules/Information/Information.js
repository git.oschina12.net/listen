define(['avalon', 'text!./Information.html','config','servers','css!./Information.css','jquery',"Jcrop"], function (avalon,html,config,servers) {
    avalon.templateCache._Information_Information = html;
     var Information = avalon.define('Information', function(vm) { 
     	vm.id = "";
     	vm.image = "";
     	vm.title = "";
     	vm.detail= "";
     	vm.content= "";
     	vm.title_tag = true;
		vm.saveArticle = function() {
			servers.httpPost('v3/article/saveArticle', {
				id : servers.GetRequest().id,
				image:vm.image,
	            title : vm.title,
				detail:vm.detail,
				content:vm.content,
			}).done(function(data) {
				if (data.code == "200"){
					avalon.router.navigate("/newsList/newsList");
				} else {
					alert(data.msg);
				}
			});	
//			}
		};
		
		vm.lookList = function() {
			servers.httpPost('v3/article/selectArticleById', {
				id : servers.GetRequest().id,
			}).done(function(data) {
				if (data.code == "200"){
					vm.title=data.body.title;
					vm.content=data.body.content;
					vm.detail=data.body.detail;
				} else {
					alert(data.msg);
				}
			});
		};
		vm.backList=function(){
			avalon.router.navigate("/newsList/newsList");
		};
		
		
		//图片插件代码
		vm.modal="modules/pluginModal/modal.html";
		vm.uploadImgSrc="modules/Information/img/introBanner.png";
    	vm.closeIn = function(){
	    	$('.selfModal').parent('div').stop().fadeIn();
	    };
	    vm.closeOut = function(){
	    	$('.selfModal').parent('div').stop().fadeOut();
	    };
		vm.changImg="modules/DeptList/img/toupdefault2.jpg";
	    vm.getImg = function(file){
			vm.changImg = file;
			$('.tishiwenzi').hide();
			$('.imginput').hide();
			$('#cropbox').Jcrop({
		      onChange: updatePreview,
		      onSelect: updatePreview,
		      aspectRatio: xsize / ysize,
		      minSize: [200,200],
		      maxSize: [10000, 10000],
		      boxWidth: 440,
		      boxHeight: 440,
		      aspectRatio:1,
		    },function(){
		      // Use the API to get the real image size
		      var bounds = this.getBounds();
		      boundx = bounds[0];
		      boundy = bounds[1];
		      $pcnt.attr("style", "width:" + 200 + "px;height:" + 200 /1 + "px");
		      // Store the API in the jcrop_api variable
		      jcrop_api = this;
		      // Move the preview into the jcrop container for css positioning
		      $preview.appendTo(jcrop_api.ui.holder);
		    });
		};
		
		vm.getQiniuImg = function(src){
			// vm.change=""
			console.log(src);
			vm.uploadImgSrc=config.qn_domain+src;
			vm.closeOut();
		};
		vm.init=function(){
			vm.title_tag = (servers.GetRequest().id == null);
			if(!vm.title_tag){
				vm.lookList();
			}
			
		};
        
//       vm.saveArticle();
	});
        servers.init(Information);                
});