define(['avalon', 'text!./addEquip.html','config','servers','css!./addEquip.css','jquery',"Jcrop"], function (avalon,html,config,servers) {
    avalon.templateCache._addEquip_addEquip = html;
     var addEquip = avalon.define('addEquip', function(vm) { 
     	vm.list={};
     	vm.url="modules/addEquip/img/pic.jpg";
     	vm.qn="";
     	vm.name="";
     	vm.id="";
     	vm.content="";
     	vm.goodsNo="";
     	vm.type="";
     	vm.applyPeople="";
     	vm.salePrice="";
     	vm.delflag="";
     	vm.introduce="";
     	vm.powerMode="";
		vm.title_tag = true;
		vm.powerMode="";
		vm.pics=[];
		vm.nullpics=["modules/addEquip/img/pic.jpg","modules/addEquip/img/pic.jpg","modules/addEquip/img/pic.jpg","modules/addEquip/img/pic.jpg","modules/addEquip/img/pic.jpg","modules/addEquip/img/pic.jpg"];
		vm.deviceList={};
		vm.model="";
		vm.brand="";
		vm.editNo=servers.GetRequest().editNo;
		
		vm.pics = [];
		
		
		
		
		vm.dsdsddsd=["sys_gravatar_2016072115355898512", "", "", "", "", ""];
		
		
		
		
		
		
     	vm.lookDevice=function(){
			servers.httpPost('v3/goods/selectDeviceById', {
				id:servers.GetRequest().id
			}).done(function(data) {
				if (data.code== "200") {
					vm.deviceList={};
					vm.deviceList=data.body;
					vm.qn = config.qn_domain;
					vm.name=vm.deviceList.name;
					vm.content=vm.deviceList.content;
					vm.type=vm.deviceList.type;
					vm.goodsNo=vm.deviceList.goodsNo;
					vm.applyPeople=vm.deviceList.applyPeople;
					vm.salePrice=vm.deviceList.salePrice;
					vm.delflag=vm.deviceList.delflag;
					vm.introduce=vm.deviceList.introduce;
					vm.brand=vm.deviceList.brand;
					vm.model=vm.deviceList.model;
					// vm.pics=vm.deviceList.pics.split(',')
					// compare();
					
				} else {
					return;
				}		
			});
		};
		
		// function compare(){
			// if(vm.deviceList.pics==""){
				// vm.pics=[];
				// vm.nullpics=new Array(6);
				// return;
			// }else{
				// vm.pics=vm.deviceList.pics.split(',');
				// vm.nullpics = [];
				// for(var i=0; i<6-vm.pics.length;i++){
					// vm.nullpics.push("");
				// }
			// }
		// };
		
		vm.picNum;
		vm.saveDevice=function(){
			vm.picNum = (vm.pics.join(',')+vm.nullpics.join(','));
			servers.httpPost('v3/goods/saveDevice', {
				id:servers.GetRequest().id,
				name:vm.name,
		     	content:vm.content,
		     	goodsNo:vm.goodsNo,
		     	type:vm.type,
		     	powerMode:vm.powerMode,
		     	applyPeople:vm.applyPeople,
		     	salePrice:vm.salePrice,
		     	delflag:vm.delflag,
		     	introduce:vm.introduce,
		     	pics:vm.picNum,
		     	brand:vm.brand,
		     	model:vm.model
			}).done(function(data) {
				if (data.code == "200") {
					avalon.router.navigate("/departList/departList");
				} else {
					alert(data.msg);
				}		
			});
		};
		if(vm.editNo==1){
			setTimeout(function(){
				$('.addEquip').css({'pointer-events':'none'});
			},500);
		}
		vm.init = function(){
        	vm.title_tag = (servers.GetRequest().id == null);
			vm.lookDevice();
      	};
      	
      	
      	//图片裁剪插件代码
      	vm.imgNum="";
      	vm.modal="modules/pluginModal/modal.html";
    	vm.uploadImgSrc="modules/addEquip/img/pic.jpg";
    	vm.closeIn = function(){
	    	$('.selfModal').parent('div').stop().fadeIn();
	    	vm.imgNum=$(this).parent().index();
	    	alert(vm.imgNum);
	    };
	    vm.closeOut = function(){
	    	$('.selfModal').parent('div').stop().fadeOut();
	    };
		vm.changImg="modules/DeptList/img/toupdefault2.jpg";
	    vm.getImg = function(file){
			vm.changImg = file;
			$('.tishiwenzi').hide();
			$('.imginput').hide();
			$('#cropbox').Jcrop({
		      onChange: updatePreview,
		      onSelect: updatePreview,
		      aspectRatio: xsize / ysize,
		      minSize: [200,200],
		      maxSize: [10000, 10000],
		      boxWidth: 440,
		      boxHeight: 440,
		      aspectRatio:1,
		    },function(){
		      // Use the API to get the real image size
		      var bounds = this.getBounds();
		      boundx = bounds[0];
		      boundy = bounds[1];
		      $pcnt.attr("style", "width:" + 200 + "px;height:" + 200 /1 + "px");
		      // Store the API in the jcrop_api variable
		      jcrop_api = this;
		      // Move the preview into the jcrop container for css positioning
		      $preview.appendTo(jcrop_api.ui.holder);
		    });
		};
		
		vm.getQiniuImg = function(src){
			// vm.change=""
			console.log(src);
			vm.uploadImgSrc=config.qn_domain+src;
			vm.pics[vm.imgNum]=vm.uploadImgSrc;
			vm.nullpics[vm.imgNum]="";
			vm.nullpics[vm.imgNum]=vm.uploadImgSrc;
			vm.closeOut();
		};
	});
      servers.init(addEquip);                
});