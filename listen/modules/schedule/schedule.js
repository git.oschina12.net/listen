define(['avalon', 'text!./schedule.html','config','servers','laydate','css!./schedule.css','jquery',], function (avalon,html,config,servers,laydate) {
    avalon.templateCache._schedule_schedule= html;
    var schedule = avalon.define('schedule', function(vm) {
    	
    	
    	vm.dateTextArray = [];
    	vm.zhanwei = [];
    	
    	
    	vm.saveSchedule = [];

//  	vm.now = laydate.now(0);
    	
    	vm.now ="";
    	
    	vm.x=1;
    	vm.save_zb = function(ischecked,work){
    		if(ischecked.ischecked == 1)
				ischecked.ischecked = 0;
			else
				ischecked.ischecked = 1;
		}
    	
    	vm.save = function(){
    		
    		vm.saveSchedule = [];
    		
    		for (var i = 0; i < vm.dateTextArray.length; i++) {
				for(var j=0;j<vm.dateTextArray[i].works.length;j++){
					
					if(vm.dateTextArray[i].works[j].id){					//判断有没有id 有id就都要push
						//vm.saveSchedule.push({"id":work.id,"datetime":work.datetime,"works":ischecked.works,"ischecked":ischecked.ischecked});
							vm.saveSchedule.push({"id":vm.dateTextArray[i].works[j].id,"datetime":vm.dateTextArray[i].datetime,"works":vm.dateTextArray[i].works[j].works,"ischecked":vm.dateTextArray[i].works[j].ischecked});
					}else{					//判断有没有id 没id ischecked = 1 都要push
						if(vm.dateTextArray[i].works[j].ischecked == 1){
							vm.saveSchedule.push({"id":vm.dateTextArray[i].works[j].id,"datetime":vm.dateTextArray[i].datetime,"works":vm.dateTextArray[i].works[j].works,"ischecked":vm.dateTextArray[i].works[j].ischecked});
						}
					}
					
				}
			}
    		
    		servers.httpPost('v3/hospitalScheduling/saveSchedulingList', {
    			schedule:JSON.stringify(vm.saveSchedule)
    		}).done(function(data) {
				if(data.code == "200"){
					
				} else {
					// alert("data.msg");
				}
			});
    	}
        	
    	vm.select = function(){
    		servers.httpPost('v3/hospitalScheduling/selectSchedulingList', {
    			dateMonth:vm.now
    		}).done(function(data) {
				if(data.code == "200"){
					for (var i = 0; i < data.body.length; i++) {
						vm.dateTextArray[parseInt(data.body[i].datetime.split("-")[2])-1].works[data.body[i].works-1].ischecked = 1;
						vm.dateTextArray[parseInt(data.body[i].datetime.split("-")[2])-1].works[data.body[i].works-1].id = data.body[i].id;
					}
				} else {
					// alert("data.msg");
				}
			});
    	}
		
		vm.schedule = function(){
			var arr = vm.now.split('-');
            var y = arr[0]; //获取当前日期的年份
            var m = arr[1];
			vm.dateTextArray = getDateText(y,m);
		}
		
		vm.daytag = 0;
		vm.pre=function(){
			if(vm.daytag > -1){
				vm.daytag--;
			}else{
				return;
			}
			
			getPreMonth(vm.now);
			vm.init1();
		};
		
		vm.next=function(){			
			if(vm.daytag < 1){
				vm.daytag++;
			}else{
				return;				
			}
			
			getNextMonth(vm.now);
			vm.init1();
		};
		/**
         * 获取上一个月
         *
         * @date 格式为yyyy-mm-dd的日期，如：2014-01-25
         */
        function getPreMonth(date) {
        	
            var arr = date.split('-');
            var year = arr[0]; //获取当前日期的年份
            var month = arr[1]; //获取当前日期的月份
            var day = arr[2]; //获取当前日期的日
            var days = new Date(year, month, 0);
            days = days.getDate(); //获取当前日期中月的天数
            var year2 = year;
            var month2 = parseInt(month) - 1;
            if (month2 == 0) {
                year2 = parseInt(year2) - 1;
                month2 = 12;
            }
            var day2 = day;
            var days2 = new Date(year2, month2, 0);
            days2 = days2.getDate();
            if (day2 > days2) {
                day2 = days2;
            }
            if (month2 < 10) {
                month2 = '0' + month2;
            }
             vm.now  = year2 + '-' + month2;
            return vm.now ;
        }
        
        /**
         * 获取下一个月
         *
         * @date 格式为yyyy-mm-dd的日期，如：2014-01-25
         */        
        function getNextMonth(date) {
            var arr = date.split('-');
            var year = arr[0]; //获取当前日期的年份
            var month = arr[1]; //获取当前日期的月份
            var day = arr[2]; //获取当前日期的日
            var days = new Date(year, month, 0);
            days = days.getDate(); //获取当前日期中的月的天数
            var year2 = year;
            var month2 = parseInt(month) + 1;
            if (month2 == 13) {
                year2 = parseInt(year2) + 1;
                month2 = 1;
            }
            var day2 = day;
            var days2 = new Date(year2, month2, 0);
            days2 = days2.getDate();
            if (day2 > days2) {
                day2 = days2;
            }
            if (month2 < 10) {
                month2 = '0' + month2;
            }
        
            vm.now = year2 + '-' + month2 ;
            return vm.now ;
        }
		
		vm.init = function(){
			vm.select();
			var arr = laydate.now(0).split('-');
            var y = arr[0]; //获取当前日期的年份
            var m = arr[1];
           vm.now = y+"-"+m;
           vm.schedule();
			
		}
		
		vm.init1 = function(){
			vm.schedule();
			vm.select();
//			var y = date.getFullYear();
//          var m = date.getMonth()+1;//获取当前月份的日期
//           vm.now = y+"-"+m;
			
		}
		
		function getDateText(Year, Month) {
			var arry = new Array();
			vm.zhanwei = [];
			for(var k=1;k<=getLastDay(Year, Month);k++){ 
				vm.zhanwei.push(i);
			};
//			schedule = [{"id":1000,"datetime":"2016-07-21","works":1,"ischecked":1 },{"id":1001,"datetime":"2016-07-22","works":2,"ischecked":0},{"id":1002,"datetime":"2016-07-22","works":3,"ischecked":1}]
			var n = getDayNumOfMonth(Year, Month);
			for (var i = 0; i < n; i++) {
				arry.push({"datetime":Year+"-"+Month+"-"+(i+1),"works":[]});
				for(var j=1;j<=3;j++){
					if(j==1){
						arry[i].works.push({"id":null,"work":j,"ischecked":0,"title":"上午","works":1});
					}
					if(j==2){
						arry[i].works.push({"id":null,"work":j,"ischecked":0,"title":"中午","works":2});
					}
					if(j==3){
						arry[i].works.push({"id":null,"work":j,"ischecked":0,"title":"下午","works":3});
					}
				}
			};
			return arry;
		}
		
		function getDayNumOfMonth(Year, Month) {
			Month--;
			var d = new Date(Year, Month, 1);
			d.setDate(d.getDate() + 32 - d.getDate());
			return (32 - d.getDate());
		}
		
		function getLastDay(Year, Month){
			var date = new Date(Year+"-"+Month+"-"+"1");
			return date.getDay();
		}
		
	}); 
	servers.init(schedule);
});