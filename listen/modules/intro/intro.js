define(['avalon','text!./intro.html','config','servers','css!./intro.css','jquery','mmRouter','map',"Jcrop"], function (avalon,page,config,servers) {
    avalon.templateCache._intro_intro= page;
    var intro = avalon.define('intro', function(vm) {
		vm.searchtext="";
		var map;
    	vm.aa = function(){
			try{
				map = new BMap.Map("allmap"); // 创建Map实例
				//map.centerAndZoom(new BMap.Point(116.404, 39.915), 15); // 初始化地图，设置中心点坐标和地图级别    
				var local = new BMap.LocalSearch(map, { renderOptions:{map: map}}); 
				if(vm.searchtext)
				local.search(vm.searchtext); 
			}catch(e){
				//TODO handle the exception
				return;
			}
    	};
    	vm.modal="modules/pluginModal/modal.html";
    	vm.uploadImgSrc="";
    	vm.closeIn = function(){
	    	$('.selfModal').parent('div').stop().fadeIn();
	    };
	    vm.closeOut = function(){
	    	$('.selfModal').parent('div').stop().fadeOut();
	    };
		vm.changImg="modules/DeptList/img/toupdefault2.jpg";
	    vm.getImg = function(file){
			vm.changImg = file;
			$('.tishiwenzi').hide();
			$('.imginput').hide();
			$('#cropbox').Jcrop({
		      onChange: updatePreview,
		      onSelect: updatePreview,
		      aspectRatio: xsize / ysize,
		      minSize: [200,200],
		      maxSize: [10000, 10000],
		      boxWidth: 440,
		      boxHeight: 440,
		      aspectRatio:1,
		    },function(){
		      // Use the API to get the real image size
		      var bounds = this.getBounds();
		      boundx = bounds[0];
		      boundy = bounds[1];
		      $pcnt.attr("style", "width:" + 200 + "px;height:" + 200 /1 + "px");
		      // Store the API in the jcrop_api variable
		      jcrop_api = this;
		      // Move the preview into the jcrop container for css positioning
		      $preview.appendTo(jcrop_api.ui.holder);
		    });
		};
		
		vm.getQiniuImg = function(src){
			// vm.change=""
			console.log(src);
			vm.uploadImgSrc=config.qn_domain+src;
			vm.closeOut();
		};
		vm.totalphone="";
    	vm.webaddress="";
    	vm.introduce="";
    	vm.route="";
    	vm.id="";
    	vm.lat="";
    	vm.lng="";
		vm.introList=function(){
			servers.httpPost('v3/hospital/selectHospitalById', {
			}).done(function(data) {
				if ("200" == data.code) {
					vm.totalphone=data.body.totalphone;
					vm.webaddress=data.body.webaddress;
					vm.introduce=data.body.introduce;
					vm.searchtext=data.body.address;
					vm.route=data.body.route;
					vm.aa();
				} else {
					return;
				}		
			});
		};
		
		vm.saveHospital=function(){
			servers.httpPost('v3/hospital/saveHospital', {
				id:vm.id,
				totalphone:vm.totalphone,
		    	webaddress:vm.webaddress,
		    	introduce:vm.introduce,
		    	address:vm.searchtext,
		    	route:vm.route,
		    	lat:vm.lat,
		    	lng:vm.lng
			}).done(function(data) {
				if ("200" == data.code) {
					
				} else {
					alert(data.msg);
				}		
			});
		};
		
		vm.init=function(){
			vm.introList();
		};
		
	 });
		servers.init(intro);
});
    
