define(['avalon', 'text!./addExpert.html','config','servers','css!./addExpert.css','jquery',"Jcrop"], function (avalon,html,config,servers) {
    avalon.templateCache._addExpert_addExpert = html;
     var addExpert = avalon.define('addExpert', function(vm) { 
     	vm.id = 0;
     	vm.businessid = "";
     	vm.title = "";
     	vm.deptid= "";
     	vm.feedback= "";
     	vm.status = 0;
     	vm.sex = "";
     	vm.phone = "";
     	vm.updateby= "";
     	vm.delflag= "";
     	vm.hospitalname = 0;
     	vm.partentid = "";
     	vm.partentname = "";
     	vm.sex = "1";
     	vm.name = "";
     	vm.title ="主任医师";
     	vm.feedback="";
     	vm.status ="";
     	vm.education ="";
     	vm.honor="";
     	vm.detail = " ";
     	vm.speciality="";
     	vm.headpic = "modules/addExpert/img/exprt.png";
        vm.title_tag = true;
        vm.arr={};
		vm.check = function() {
			servers.httpPost('v3/doctor/selectDoctor', {
	     	id:servers.GetRequest().id,
			}).done(function(data) {
				if (data.code == "200"){
					vm.arr={};
					vm.arr= data.body;
				} else {
					alert(data.msg);
				}
			});
		};
		vm.add = function() {
			servers.httpPost('v3/doctor/save', {
			name:vm.arr.name,
	     	title:vm.arr.title,
	     	headpic:vm.headpic,
	     	detail:vm.arr.detail,
	     	sex:vm.arr.sex ,
	     	education:vm.arr.education ,
	     	honor:vm.arr.honor,
	     	speciality:vm.arr.speciality,
	     	id:servers.GetRequest().id
			}).done(function(data) {
				if (data.code == "200"){
					avalon.router.navigate("/mavinList/mavinList");
				} else {
					alert(data.msg);
				}
			});
		};
		vm.modal="modules/pluginModal/modal.html";
    	vm.closeIn = function(){
	    	$('.selfModal').parent('div').stop().fadeIn();
	    };
	    vm.closeOut = function(){
	    	$('.selfModal').parent('div').stop().fadeOut();
	    };
		vm.changImg="modules/DeptList/img/toupdefault2.jpg";
	    vm.getImg = function(file){
			vm.changImg = file;
			$('.tishiwenzi').hide();
			$('.imginput').hide();
			$('#cropbox').Jcrop({
		      onChange: updatePreview,
		      onSelect: updatePreview,
		      aspectRatio: xsize / ysize,
		      minSize: [200,200],
		      maxSize: [10000, 10000],
		      boxWidth: 440,
		      boxHeight: 440,
		      aspectRatio:1,
		    },function(){
		      // Use the API to get the real image size
		      var bounds = this.getBounds();
		      boundx = bounds[0];
		      boundy = bounds[1];
		      $pcnt.attr("style", "width:" + 200 + "px;height:" + 200 /1 + "px");
		      // Store the API in the jcrop_api variable
		      jcrop_api = this;
		      // Move the preview into the jcrop container for css positioning
		      $preview.appendTo(jcrop_api.ui.holder);
		    });
		};
		
		vm.getQiniuImg = function(src){
			// vm.change=""
			vm.headpic=config.qn_domain+src;
			console.log(src);
			vm.closeOut();
		};
		vm.init= function(){
			vm.title_tag = (servers.GetRequest().id == null);
			vm.check();
		};
	});
        servers.init(addExpert);                
});