define(['avalon', 'text!./DeptList.html','config','servers','css!./DeptList.css','jquery','Jcrop'], function (avalon,html,config,servers) {
    avalon.templateCache._DeptList_DeptList = html;
     var DeptList = avalon.define('DeptList', function(vm) { 
     	vm.paging = "modules/pluginPaging/paging.html";
		vm.newAnnouncement = function(){
	    	avalon.router.navigate("/newAnnouncement/newAnnouncement");
	    };
	    
	    vm.pageIndex="";
		vm.title="";
		vm.list=[];
		vm.Dep=function(){
			servers.httpPost('v3/messages/list', {
				pageIndex:vm.pageIndex,
				title:vm.title
			}).done(function(data) {
				if ("200" == data.code) {
					vm.list=[];
					vm.list=data.body;
					avalon.vmodels.root.paging(data.paginator);
				} else {
					alert(data.msg);
				}		
			});
		};
		vm.lookDetail=function(mid){
			avalon.router.navigate("/newAnnouncement/newAnnouncement?id="+mid);
		};
		vm.removeDetail=function($index, $remove,mid){
			servers.httpPost('v3/messages/deleteById', {
				id:mid,
			}).done(function(data) {
				if (data.code == "200") {
					$remove();
				} else {
					alert(data.msg)

				}		
			});
		};
		vm.init=function(){
			vm.Dep();//重新加载列表的函数
		};
		vm.paging_page = function(){
			vm.Dep();
		};
	    
	    //图片裁剪插件代码
	    // vm.modal="modules/pluginModal/modal.html";
	    // vm.closeIn = function(){
	    	// $('.selfModal').parent('div').stop().fadeIn();
	    // };
	    // vm.closeOut = function(){
	    	// $('.selfModal').parent('div').stop().fadeOut();
	    // };
	    // vm.changImg="modules/DeptList/img/toupdefault2.jpg";
	    // vm.getImg = function(file){
			// vm.changImg = file;
			// $('.tishiwenzi').hide();
			// $('.imginput').hide();
			// $('#cropbox').Jcrop({
		      // onChange: updatePreview,
		      // onSelect: updatePreview,
		      // aspectRatio: xsize / ysize,
		      // minSize: [200,200],
		      // maxSize: [10000, 10000],
		      // boxWidth: 440,
		      // boxHeight: 440,
		      // aspectRatio:1,
		    // },function(){
		      // // Use the API to get the real image size
		      // var bounds = this.getBounds();
		      // boundx = bounds[0];
		      // boundy = bounds[1];
		      // $pcnt.attr("style", "width:" + 200 + "px;height:" + 200 /1 + "px");
		      // // Store the API in the jcrop_api variable
		      // jcrop_api = this;
// 				
		      // // Move the preview into the jcrop container for css positioning
		      // $preview.appendTo(jcrop_api.ui.holder);
		    // });
		// };
		// vm.getQiniuImg = function(src){
			// // vm.change=""
			// console.log(src);
		// };
		
	});
	servers.init(DeptList);//最上面的函数
});