define(['avalon', 'text!./pushList.html','config','servers','css!./pushList.css','jquery',], function (avalon,html,config,servers) {
    avalon.templateCache._pushList_pushList= html;
    var pushList = avalon.define('pushList', function(vm) {
		vm.title="";
		vm.pageIndex="1";
		vm.status="";
		vm.id="";
		vm.list=[];
     	vm.pushList = function() {
			servers.httpPost('v3/notification/list', {
				title : vm.title,
				pageIndex:vm.pageIndex,
				status:vm.status
			}).done(function(data) {
				if (data.code == "200") {
					vm.list = data.body;
				} else {
					alert(data.msg);
				}
			});
		};
		
		vm.addMess = function(){
	    	avalon.router.navigate("/addMess/addMess");
	   	};
	   	vm.editMess = function(id){
	    	avalon.router.navigate("/addMess/addMess?id="+id);
	   	};
        vm.removeRemote = function($index, $remove,id) {
			servers.httpPost('v3/notification/deleteById', {
				id : id,
			}).done(function(data) {
				if (data.code == "200") {
                	$remove();
				}else{
					alert(data.msg);
				}
			});
    	};
    	
    	vm.init=function(){
    		 vm.pushList();
    	};
	}); 
	servers.init(pushList);
});