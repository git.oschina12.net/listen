define(['avalon', 'text!./details.html','config','servers','css!./details.css','jquery',], function (avalon,html,config,servers) {
    avalon.templateCache._details_details = html;
     var details = avalon.define('details', function(vm) { 
     	vm.list={};
     	vm.details=function(){
     		servers.httpPost('v3/suggest/selectById', {
				sid:servers.GetRequest().sid
			}).done(function(data) {
				if ("200" == data.code) {
					vm.list={};
					vm.list=data.body;
				} else {
					alert(data.msg);
				}		
			});
     	};
     	
     	vm.backList=function(){
     		avalon.router.navigate("/suggestList/suggestList");
     	};
     	
     	vm.init=function(){
     		vm.details();
     	};
	});
       servers.init(details);                 
});