define(['avalon', 'text!./suggestList.html','config','servers','css!./suggestList.css','jquery','laydate'], function (avalon,html,config,servers,laydate) {
    avalon.templateCache._suggestList_suggestList = html;
    var suggestList = avalon.define('suggestList', function(vm) {
    	vm.tabs = ["tab0", "tab1"];
		lang = [];
		vm.sids = [];
		vm.sid="";
		vm.delflag = "t";
		vm.select_all=0;
	    vm.currentIndex=0;
	    vm.paging = "modules/pluginPaging/paging.html";
        vm.toggle=function(index,tag) {
        	if(index==0){
        		$('.btnRead').show();
        	}else{
        		$('.btnRead').hide();
        	}
           vm.currentIndex = index;
           $(this).addClass("tabCurrent").siblings().removeClass("tabCurrent");
           vm.delflag = tag;
           vm.suggest();
        };
        vm.click = function(id){
	    	avalon.router.navigate("/details/details?sid="+id);
	    };
	    
	    vm.startTime="";
	    vm.endTime="";
	    vm.telphone="";
	    vm.list=[];
	    vm.suggest=function(){
	    	servers.httpPost('v3/suggest/list', {
				startTime:vm.startTime,
				endTime:vm.endTime,
				telphone:vm.telphone,
				delflag:vm.delflag
			}).done(function(data) {
				if (data.code=="200") {
					vm.list=[],
					vm.list=data.body;
					avalon.vmodels.root.paging(data.paginator);//?
				} else {
					return;
				}		
			});
	 	};
	 	vm.del = function($index, $remove,sid) {
			servers.httpPost('v3/suggest/deleteById', {
				sid: sid,
			}).done(function(data) {
				if(data.code == "200") {
					$remove();
					vm.suggest();
				} else {
					alert(data.msg);
				}
			});
		};
	 	vm.allDel = function(){
        	servers.httpPost('v3/suggest/deleteByBacthIds', {
				sids : vm.selected.join(",")//转化成字符串形式返回给后台
			}).done(function(data) {
				if (data.code == "200") {
					$remove();
					alert("成功");
				} else {
					alert(data.msg);
				}
			});
       };
       vm.allRead = function(){
        	 servers.httpPost('v3/suggest/updateByBacthIds', {
				sids :vm.selected.join(",")
			}).done(function(data) {
				if (data.code == "200") {
					alert("成功");
				} else {
					alert(data.msg);
				}
			});
       };
		vm.selected=[];//保存勾选的选项的id,方便传给后台
	    vm.listArr=[];
	    vm.select_all_cb=function(){//全选框change事件回调
	        var listArr=vm.list,selected=vm.selected;
	        if(this.checked){
				avalon.each(vm.list,function(i,v){//循环保存着已经勾选选框的数据
					selected.ensure(v['sid']);//如果里面没有当前选框的数据，就保存
				});
	        }else
	        selected.clear();//清空
	    };
	    vm.init=function(){
	    	vm.suggest();
	    };
	   	vm.paging_page = function(){
			vm.suggest();
		};
	});
	suggestList.selected.$watch('length', function(after) { //监听保存数据数组的变化
		var len = suggestList.list.length;
		if(after == len) //子选框全部被勾选
			suggestList.select_all = 1;
		else //子选框有一个没有被勾选
			suggestList.select_all = 0;
	});
    servers.init(suggestList);
//	avalon.scan();
});