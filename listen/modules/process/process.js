define(['avalon', 'text!./process.html','config','servers','css!./process.css','jquery',], function (avalon,html,config,servers) {
    avalon.templateCache._process_process = html;
     var process = avalon.define('process', function(vm) { 
		vm.edit="modules/pluginEdit/edit.html";
		vm.modal="modules/pluginModal/modal.html";
		vm.modalIn = function(){
			vm.closeIn();
		};
		vm.textIn = function(){
			$('.wordEdit').show();
			vm.text="";
		};
		vm.closeIn = function(){
	    	$('.selfModal').parent('div').stop().fadeIn();
	    };
	    vm.closeOut = function(){
	    	$('.selfModal').parent('div').stop().fadeOut();
	    };
	    vm.text="";
	    vm.changeText="";
	    vm.wordIn= function(){
	    	vm.changeText=vm.text;
	    	$('.wordEdit').hide();
	    };
	    vm.wordOut= function(){
	    	vm.text="";
	    };
	    
	    vm.id="";
	    vm.infuse="";
	    vm.flowList = function() {
			servers.httpPost('v3/appimage/selectFittingFlow', {
			}).done(function(data) {
				if (data.code == "200") {
					vm.infuse = data.body.infuse;
					vm.id=data.body.id;
				} else {
					
				}
			});
		};
		
		vm.saveFlow = function() {
			servers.httpPost('v3/appimage/saveFittingFlow', {
				id:vm.id,
				infuse:vm.infuse
			}).done(function(data) {
				if (data.code == "200") {
					
				} else {
					alert(data.msg);
				}
			});
		};
		vm.init=function(){
			vm.flowList();
		};
	});
       servers.init(process);                 
});