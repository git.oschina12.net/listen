define('servers' ,['config','security','Math','jquery',],function (config) {
	
	// 随即获取uuid
	var uuid = Math.uuid(16);
	// 将获取到的uuid存储到本地
	var storage = window.localStorage;
	if (!storage.getItem('uuid')) {
	    storage.setItem("uuid", uuid);
	}
	
	/**
	 * 登录成功之后处理返回data
	 */
//	var loginSuccess = function(data){
//		storage.setItem("token", data.token);
//		storage.setItem("userinfo", JSON.stringify(data.userinfo));
//	};
	
	
	var authInfo= function(data){
		storage.setItem("authInfo",data);
	};
	
	var getUser= function(){
		window.userId = JSON.parse(storage.getItem('authInfo')).userId;
		return JSON.parse(storage.getItem('authInfo'));
	};
	
	/**
	 * post请求
	 * @param {Object} url
	 * @param {Object} json
	 */
	
	var flag=false;
	var httpPost1 = function(url,json){
		var param;
		if(typeof(json) == "undefined"){
			param = {token:storage.getItem('token')};
		}else{
			param=json;
			param.token = storage.getItem('token');
		}
		var dtd = $.Deferred(); // 新建一个Deferred对象
		// $.post(config.url + url,param, function(data) {
				// vifity(data.code,data.msg);
				// data = JSON.parse(data);
				// var token = data.token;
				// if(typeof(token) != "undefined"){
					// storage.setItem("token", token);
				// }
				// dtd.resolve(data); 
		// }).error(function(data) {
			// if(flag)
				// alert("连接服务器异常["+data.status+"],请检查网络设置或稍后重试!"); 
		// });
		// return dtd.promise();
		$.ajax(config.url + url, {
		    type: 'post',
		    data: param, 
		    headers: { 
		    	"Authorization": storage.getItem('token'),
		    	"AuthInfo":storage.getItem('authInfo'),
		    },
		    success: function(data){
		    	// data = JSON.parse(data);
		    	try {
					data = JSON.parse(data);
				} catch (e) {
					console.log("返回格式为对象，不用处理！");
				}

		    	//vifity(data.code,data.msg);
				var token = data.token;
				if(typeof(token) != "undefined"){
					storage.setItem("token", token);
				}
				dtd.resolve(data); 
		    },
		    error: function(data){
		    	alert("连接服务器异常["+data.status+"],请检查网络设置或稍后重试!"); 
		    }
		});
		return dtd.promise();
	};
	
	
	
	var httpPost = function(url,json){
		var param;
		if(typeof(json) == "undefined"){
			param = {token:$.session.get('token')};
		}else{
			param=json;
			param.token = $.session.get('token');
		}
		var dtd = $.Deferred(); // 新建一个Deferred对象
		// $.post(config.url + url,param, function(data) {
				// vifity(data.code,data.msg);
				// data = JSON.parse(data);
				// var token = data.token;
				// if(typeof(token) != "undefined"){
					// storage.setItem("token", token);
				// }
				// dtd.resolve(data); 
		// }).error(function(data) {
			// if(flag)
				// alert("连接服务器异常["+data.status+"],请检查网络设置或稍后重试!"); 
		// });
		// return dtd.promise();
		$.ajax(config.back_url + url, {
		    type: 'post',
		    data: param, 
		    headers: { 
		    	"Authorization": $.session.get('token'),
		    	"AuthInfo":$.session.get('authInfo'),
		    },
		    success: function(data){
		    	// data = JSON.parse(data);
		    	try {
					data = JSON.parse(data);
				} catch (e) {
					console.log("返回格式为对象，不用处理！");
				}

		    	//vifity(data.code,data.msg);
				var token = data.token;
				if(typeof(token) != "undefined"){
					$.session.set('token', token);
				}
				
				dtd.resolve(data); 
		    },
		    error: function(data){
		    	alert("连接服务器异常["+data.status+"],请检查网络设置或稍后重试!"); 
		    }
		});
		return dtd.promise();
	};
	/**
	 * get请求
	 * @param {Object} url
	 * @param {Object} json
	 */
	var httpGet = function(url,json){
		var param;
		if(typeof(json) == "undefined"){
			param = {token:$.session.get('token')};
		}else{
			param=json;
			param.token = $.session.get('token');
		}
		//param = json;
		var dtd = $.Deferred(); // 新建一个Deferred对象
		
//		$.get(config.url + url,param, function(data) {
//				vifity(data.code,data.msg);
//				data = JSON.parse(data);
//				var token = data.token;
//				if(typeof(token) != "undefined"){
//					storage.setItem("token", token);
//				}
//				dtd.resolve(data); 
//		})
//		.error(function(data) {
//			alert("连接服务器异常["+data.status+"],请检查网络设置或稍后重试!");
//		});

		$.ajax(config.back_url + url, {
		    type: 'get',
		    data: param, 
		    headers: { 
		    	"Authorization": $.session.get('token'),
		    	"AuthInfo":$.session.get('authInfo'),
		    },
		    success: function(data){
		    	try {
				 	data = JSON.parse(data);
				} catch (e) {
					console.log("返回格式为对象，不用处理！");
				}
		    	//vifity(data.code,data.msg);
				var token = data.token;
				if(typeof(token) != "undefined"){
					//storage.setItem("token", token);
					$.session.set('token', token);
				}
				dtd.resolve(data); 
		    },
		    error: function(e){
		    	alert("连接服务器异常["+e.status+"],请检查网络设置或稍后重试!"); 
		    }
		});

		return dtd.promise();
	};
	//RSA加密密码，获取公钥
	var RSA = function(password){
		var publicKeyExponent, publicKeyModulus;
		$.ajax({ //这里需要ajax同步
			type: 'POST',
			async: false,
			url: config.url + 'api/getPublicKey',
			data: {
				uuid: storage.getItem('uuid')
			},
			success: function(data) {
				data = JSON.parse(data);
				publicKeyExponent = data.publicKeyExponent;
				publicKeyModulus = data.publicKeyModulus;
			},
			dataType: 'json'
		});
		RSAUtils.setMaxDigits(200);
		//setMaxDigits(256);
		var key = new RSAUtils.getKeyPair(publicKeyExponent, "", publicKeyModulus);
		return RSAUtils.encryptedString(key, password);
	};

	/**
	* 验证码
	*/
	var reloadcode =function() {
		// 随即获取uuid
		var uuid = Math.uuid(16);
		// 将获取到的uuid存储到本地
		storage.setItem("uuid", uuid);
		return config.url + 'api/yzimage?uuid=' + storage.getItem('uuid') + "&m=" + Math.random();
	};
	
	
	/**
	 * 权限控制
	 */
	function vifity(code,msg){
		if(code=="2003"||code=="2001"||code=="2002"){
			if(flag){
				alert(msg);
			}
			exittologin1();
		}
	}
//	var loginSuccess=function(data){
//		storage.setItem("token", data.token);
//		storage.setItem("userinfo", JSON.stringify(data.userinfo));
//	}

	/**
	 * center.html
	 * 退出登录
	 */
	function exittologin(){
//		storage.removeItem("token");
//		storage.removeItem("userinfo");
		$.session.clear();
		location.href = "login/register.html";
	}
	
	/**
	 * 
	 * 退出登录
	 */
	function exittologin1(){
//		storage.removeItem("token");
//		storage.removeItem("userinfo");
		$.session.clear();
		location.href = "../login/register.html";
	}
	
	/**
	 * 
	 * 初始化
	 */
	window.addEventListener("hashchange", hashchange);
	var map = []; 
	var init = function(page){
	    map[window.init_page] = page;
	    if(map[window.init_page] && map[window.init_page].init)
	    map[window.init_page].init();
    };
    
    function  hashchange(){
    	if(map[window.init_page] && map[window.init_page].init)
		map[window.init_page].init();
    }
    
    /**
	 * 获取url参数
	 */
	var GetRequest = function() {
	    var url = "?"+location.hash.split('?')[1]; //获取url中"?"符后的字串
	    var theRequest = new Object();
	    if (url.indexOf("?") != -1) {
	        var str = url.substr(1);
	        strs = str.split("&");
	        for(var i = 0; i < strs.length; i ++) {
	            theRequest[strs[i].split("=")[0]] = unescape(strs[i].split("=")[1]);
	        }
	    }
	    return theRequest;
	};
	
    /**
	 * 
	 * 暴露出去的服务,如果不暴露,所有的方法属性都是私有的外部不能访问和修改.
	 */
	return {
		httpPost : httpPost,
		httpPost1 : httpPost1,
		httpGet : httpGet,
		storage : storage,
		reloadcode : reloadcode,
		RSA : RSA,
		init : init,
		GetRequest : GetRequest,
		//loginSuccess : loginSuccess,
		authInfo:authInfo,
		getUser:getUser,
	};
	
});