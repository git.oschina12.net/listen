define('init' ,['config','servers','jquery','jqSession'],function (config,servers) {
	
	//$.session.set('token', data.token);
	//$.session.set('userinfo', JSON.stringify(data.userinfo));
	
	//$.session.get('xxxx');
	//$.session.set('xxxx','oooo');
	
	$.session.set('back_url', config.back_url);
	$.session.set('url', config.url);
	$.session.set('qn_domain', config.qn_domain);
	$.session.set('weburl', config.weburl);
	
	if(servers.GetRequest().appId){
		$.session.set('appId', servers.GetRequest().appId);
	}
    	
    if(servers.GetRequest().token){
    	$.session.set('token', servers.GetRequest().token);
    }
	
	servers.httpPost('v3/toSys/getAutoInfo', {
		appId : $.session.get('appId'),
		token : $.session.get('token')
	}).done(function(data) {
		if ("200" == data.code) {
			$.session.set('hosiptalId', data.authInfo.hosiptalId);
			$.session.set('userId', data.authInfo.hosiptalId);
			$.session.set('authInfo', data.authInfo);
		} else {
			return;
		}		
	});
});